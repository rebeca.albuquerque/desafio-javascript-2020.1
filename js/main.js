alert("Teste seus conhecimentos sobre os Valores da GTi <3")

//------------JSON----------
let Questions

httpRequest = new XMLHttpRequest();

httpRequest.open('GET', 'https://quiz-trainee.herokuapp.com/questions', true);
httpRequest.send();

httpRequest.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        Questions = (JSON.parse(this.responseText));
    }
}

//---------DECLARAÇÃO DE VARIAVEIS E CONSTANTES--------
let currentQuestion = -1
let score = 0
const startButton = document.getElementById('start-btn')
const tituloElement = document.getElementById('titulo')
const listaRespostasElement = document.getElementById('listaRespostas')

startButton.addEventListener('click', mostrarQuestao)

function mostrarQuestao() {
    tituloquiz.classList.add('hide')
    let spans = document.getElementsByTagName('span');
    let spansRespostas = document.getElementsByName('resposta');
    document.getElementById('listaRespostas').style.display = 'block';
    
    if(spansRespostas[0].checked != false || spansRespostas[1].checked != false || spansRespostas[2].checked != false || spansRespostas[3].checked != false || currentQuestion ==-1){ //Só permite que avance se uma resposta for marcada
        if(currentQuestion != -1){
            for(var i=0; i<spansRespostas.length; i++){ //contar os pontos
                score += spansRespostas[i].checked*Questions[currentQuestion]['options'][i]['value'];
            }
        }
        document.getElementById("start-btn").innerHTML = "Próxima" //"troca" o botão start pelo "Próxima"
        currentQuestion++; //incrementa valor a questão para avançar
    }

    console.log(Questions);//teste

    if (currentQuestion == -1) {//Remove as perguntas e repostas antes de iniciar o quiz
        tituloElement.classList.remove('hide')
        listaRespostasElement.classList.remove('hide')
    }

    console.log(currentQuestion)//teste

    if(currentQuestion<Questions.length){ 
        document.getElementById('titulo').innerHTML = Questions[currentQuestion].title; //atualiza os titulos das perguntas
    for (let i = 0; i < Questions[currentQuestion].options.length; i++) {
        if (spansRespostas[i].checked) { 
            spansRespostas[i].checked = false; //desmarca o item da questão anterior
        }
        spans[i].innerHTML = Questions[currentQuestion].options[i].answer;//atualiza as repostas e seus respectivos valores
        spansRespostas[i].value = Questions[currentQuestion].options[i].value; //""
    }}
    else{
        finalizarQuiz();
    }

}

function finalizarQuiz() {
    tituloquiz.classList.remove('hide')
    let resultado = (score / (3 * Questions.length)) * 100
    document.getElementById('listaRespostas').style.display = 'none'
    currentQuestion = -1;
    score = 0;
    document.getElementById('titulo').innerHTML = resultado + "%";
    document.getElementById('start-btn').innerHTML = "Refazer"
    document.getElementById('tituloquiz').innerHTML = "Seu Resultado é:"
}
